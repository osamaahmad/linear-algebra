#pragma once

#include "Matrix2.hpp"
#include "Vector.hpp"

template <typename T>
struct EigenPair
{
    Vector<T> eigenvector;
    T eigenvalue;
};

static const int default_iterations = 1000;

template <typename T>
Vector<T> eigenvector_iterative(const Matrix2<T>& matrix, int iterations = default_iterations)
{
    // Power iteration method
    //  This returns only the greatest eigenvector.
    //  The idea here is that by applying the transformation
    //   multiple times to a random vector, the vector will
    //   eventually converge to the dominant eigenvector of
    //   the provided matrix.

    if (!matrix.is_square())
        throw std::invalid_argument("Can't compute the eigenvector for a non-square matrix");

    // Can be set to any random value.
    Vector<T> result(matrix.rows());
    for (int i = 0; i < result.size(); i++)
        result[i] = 1;

    while (iterations--)
        result = (matrix * result).normalize();

    return result;
}

template <typename T>
EigenPair<T> eigenvalue_iterative(const Matrix2<T>& matrix, int iterations = default_iterations)
{
    EigenPair<T> result;
    result.eigenvector = eigenvector_iterative(matrix, iterations);

    Vector<T>& vector = result.eigenvector;
    T& value = result.eigenvalue;

    // Solving only the first equation.
    T sum = 0;
    for (int i = 1; i < vector.size(); i++)
        sum += matrix[0][i] * vector[i];

    value = (sum / vector[0]) + matrix[0][0];

    return result;
}