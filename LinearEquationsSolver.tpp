#include "Matrix2.hpp"
#include <cmath>
#include <stdexcept>

// TODO find a better name for the class.
//  The class is also used to compute the
//  inverse of a matrix, and to compute
//  the rank.

template <typename T>
class LinearEquationsSolver
{
    int rank = 0;
    int unknowns = 0;
    int current_row = 0;
    int current_col = 0;
    Matrix2<T>* matrix;

    int get_non_zero_row_index()
    {
        auto column = matrix->column(current_col);

        for (int i = current_row; i < column.size(); i++) {
            if (column[i] * column[i] > 1e-18) {
                return i;
            }
        }

        return current_row;
    }

    bool set_pivot_to_1()
    {
        // is a reference so that we don't have to
        //  reassign in the case of swapping.
        T& current_element = (*matrix)[current_row][current_col];

        if (current_element == 1) {
            return true;
        }

        int non_zero_row = get_non_zero_row_index();

        if (current_row != non_zero_row)
            matrix->row(current_row).swap_with_row(non_zero_row);

        if (current_element * current_element < 1e-18) {
            return false;
        }

        matrix->row(current_row) /= current_element;

        return true;
    }

    void set_rest_of_column_to_0()
    {
        for (int next_row = current_row + 1; next_row < matrix->rows(); next_row++)
        {
            T multiplier = -(*matrix)[next_row][current_col];
            matrix->row(next_row).add_row(current_row, multiplier);
        }
    }

    void set_to_row_echelon_form()
    {
        // TODO make sure that the computation of the rank is correct.

        rank = 0;
        current_row = 0;
        current_col = 0;

        while (current_row < matrix->rows() && current_col < unknowns)
        {
            // If there is at least a single row for
            //  which we can't set the pivot to be 1,
            //  in other words, the matrix is not full
            //  rank, then the matrix can't be in row
            //  echelon form, and as a consequence, the
            //  system of linear equations doesn't have
            //  a single solution. We can break out of
            //  the loop once we find that this is the
            //  case, but we have to keep iterating for the
            //  rank of the matrix (the number of equations
            //  that are not a linear combination (linearly
            //  independent) of each other) to be correct.

            if (set_pivot_to_1()) {
                set_rest_of_column_to_0();
                current_row++;
                rank++;
            }

            current_col++;
        }
    }

    void reduce()
    {
        // We only reduce in case we're solving a system with
        //  a unique solution, which means that the matrix is
        //  full rank, therefore, we can assume that we'll always
        //  have number of linearly independent equations >= the
        //  number of unknowns, which makes the code simpler.

        for (current_row = matrix->rows() - 2; current_row >= 0; current_row--)
        {
            for (int next_row = current_row + 1; next_row < unknowns; next_row++)
            {
                int column = next_row;
                T multiplier = -(*matrix)[current_row][column];
                matrix->row(current_row).add_row(next_row, multiplier);
            }
        }
    }

    void check(const Matrix2<T>& coefficients, const Matrix2<T>& solution)
    {
        if (coefficients.rows() < coefficients.columns())
            throw std::invalid_argument("There are no enough equations");

        if (coefficients.rows() != solution.rows())
            throw std::invalid_argument("The number of rows is not equal in the two matrices");
    }

public:

    Matrix2<T> solve(Matrix2<T> coefficients, Matrix2<T> constants)
    {
        // For a system of equations with n
        //  unknowns (n columns), in case the
        //  number of equations (the number of rows)
        //  is more than the n, the returned
        //  matrix will have the same dimensions,
        //  with the first n rows being the solution.

        check(coefficients, constants);

        Matrix2<T> augmented = coefficients.augmented_with(constants);
        matrix = &augmented;

        unknowns = coefficients.columns();

        set_to_row_echelon_form();
        if (rank != unknowns)
            throw std::invalid_argument("The system doesn't"
                " have a single solution (Rank = " + std::to_string(rank) + ")");

        reduce();

        return matrix->split_right(unknowns);
    }

    void set_row_echelon(Matrix2<T>& matrix)
    {
        this->matrix = &matrix;
        unknowns = matrix.columns();
        set_to_row_echelon_form();
    }

    int compute_rank(Matrix2<T> matrix)
    {
        set_row_echelon(matrix);
        return rank;
    }
};


// TODO figure out a way to return the rank along
//  with the matrix after setting it to row echelon form.

template <typename T>
Matrix2<T> Matrix2<T>::solved(const Matrix2<T>& constants) const
{
    return LinearEquationsSolver<T>{}.solve(*this, constants);
}

template <typename T>
Matrix2<T> Matrix2<T>::inverse() const
{
    if (!is_square())
        throw std::invalid_argument("Only square matrices can have an inverse.");

    return LinearEquationsSolver<T>{}.solve(*this, identity());
}

template <typename T>
Matrix2<T>& Matrix2<T>::set_row_echelon()
{
    LinearEquationsSolver<T>{}.set_row_echelon(*this);
    return *this;
}

template <typename T>
int Matrix2<T>::rank() const
{
    return LinearEquationsSolver<T>{}.compute_rank(*this);
}

template <typename T>
SolutionType Matrix2<T>::solution_type(const Matrix2<T>& constants) const
{
    int unknowns = columns();
    int coefficients_rank = rank();

    // We don't care about the state of the augmented
    //  matrix, and there is no need to copy it.
    auto augmented = this->augmented_with(constants);
    int augmented_rank = LinearEquationsSolver<T>{}.compute_rank(std::move(augmented));

    if (coefficients_rank == augmented_rank) {
        if (coefficients_rank == unknowns)
            return SolutionType::UNIQUE_SOLUTION;

        // if coefficients_rank < unknowns
        return SolutionType::INFINITE_SOLUTIONS;
    }

    // if coefficients_rank < augmented_rank
    return SolutionType::NO_SOLUTION;
}