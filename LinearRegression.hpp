#pragma once

#include <vector>
#include "Matrix2.hpp"
#include "Vector.hpp"

template <typename T>
struct LineInfo
{
    T slope;
    T y_intercept;
};

template <typename T>
struct Point
{
    T x;
    T y;
};

template <typename T>
LineInfo<T> linear_least_squares(const std::vector<Point<T>>& points)
{
    int n = points.size();

    T *x_arr = new T[n * 2];
    {
        int i = 0;
        for (auto& p : points) {
            x_arr[i++] = 1;
            x_arr[i++] = p.x;
        }
    }

    T* y_arr = new T[n];
    for (int i = 0; i < n; i++) {
        y_arr[i] = points[i].y;
    }

    // TODO there is no need to copy the
    //  data again, you can just "steal"
    //  the pointer to the data.
    Matrix2<T> x(points.size(), 2, x_arr);
    Vector<T> y(points.size(), y_arr);
    delete[] x_arr;
    delete[] y_arr;

    Matrix2<T> xt = x.transpose();

    Matrix2<T> result = (xt * x).inverse() * xt * y;


    return { result[0][1], result[0][0] };
}
