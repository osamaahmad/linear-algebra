#pragma once

enum class SolutionType
{
    NO_SOLUTION = 0, UNIQUE_SOLUTION = 1, INFINITE_SOLUTIONS = 2
};

template <typename T>
class Vector;

template <typename T>
class MatrixRow;

template <typename T>
class MatrixColumn;

template <typename T>
class Matrix2
{
    friend class Vector<T>;

    int rows_count;
    int columns_count;
    T* data;

public:

    Matrix2() : rows_count(0), columns_count(0), data(nullptr) {}

    Matrix2(int rows_count, int columns_count)
        : rows_count(rows_count), columns_count(columns_count), data(new T[size()])
    {
        reset();
    }

    template <typename U>
    Matrix2(int rows_count, int columns_count, U array)
        : rows_count(rows_count), columns_count(columns_count),
              data(new T[size()])
    {
        for (int i = 0; i < size(); i++)
            data[i] = array[i];
    }

    template <typename U>
    Matrix2(const Matrix2<U>& matrix)
            : rows_count(matrix.rows()), columns_count(matrix.columns()),
              data(new T[size()])
    {
        for (int i = 0; i < size(); i++)
            data[i] = matrix.linear_array()[i];
    }

    Matrix2(const Matrix2<T>& matrix)
        : rows_count(matrix.rows()), columns_count(matrix.columns()),
          data(new T[size()])
    {
        for (int i = 0; i < size(); i++)
            data[i] = matrix.linear_array()[i];
    }

    Matrix2(Matrix2<T>&& matrix) noexcept
    {
        data = matrix.data;
        matrix.data = nullptr;

        rows_count = matrix.rows();
        columns_count = matrix.columns();
    }

    ~Matrix2() { delete[] data; }

    Matrix2<T> inverse() const;

    Matrix2<T> solved(const Matrix2<T>& constants) const;

    Matrix2<T>& set_row_echelon();

    int rank() const;

    SolutionType solution_type(const Matrix2<T>& constants) const;

    bool is_full_rank() const { return rank() == columns(); }

    T determinant() const;

    Matrix2<T> transpose() const;

    static Matrix2<T> identity(int n);
    Matrix2<T> identity() const;

    template <typename U>
    Matrix2<T> augmented_with(const Matrix2<U>& rhs) const;

    Matrix2<T> split_left(int split_point) const;
    Matrix2<T> split_right(int split_point) const;

    MatrixRow<T> row(int row_index);
    MatrixColumn<T> column(int column_index);

    void reset();

    bool is_square() const { return rows() == columns(); }

    template <typename U>
    bool has_same_dimensions(const Matrix2<U>& matrix) const {
        return    rows() == matrix.rows() &&
               columns() == matrix.columns();
    }

    template <typename U>
    bool is_multipliable(const Matrix2<U>& matrix) const {
        return columns() == matrix.rows();
    }

    T* linear_array() { return data; }
    const T* linear_array() const { return data; }

    int rows() const { return rows_count; }
    int columns() const { return columns_count; }
    int size() const { return rows() * columns(); }

    operator Vector<T>() const;

    Matrix2<T>& operator=(const Matrix2<T>& rhs);
    Matrix2<T>& operator=(Matrix2<T>&& rhs) noexcept;
    template<typename U> Matrix2<T>& operator=(const Matrix2<U>& rhs);

    template<typename U> bool operator ==(const Matrix2<U>& rhs) const;

    template<typename U>
    bool operator !=(const Matrix2<U>& rhs) const { return *this != rhs; }

    template<typename U>
    bool is_equal(const Matrix2<U>& rhs, double tolerance = 1e-9) const;

    T* operator[](int row);
    const T* operator[](int row) const;

    Matrix2<T> operator-() {
        return Matrix2(*this) * -1;
    }

// TODO get rid of the code duplication
//  {

    // Note:
    //  For data types T and U, U * Matrix2<T> will result in
    //   Matrix2<U>, while Matrix<T> * U will result in Matrix2<T>.
    //   Same goes for /, +, and - as well.
    //  Also, Matrix2<U> * Matrix2<T> = Matrix2<U> while
    //   Matrix2<T> * Matrix2<U> = Matrix2<T>.
    //   Same goes for /, +, and - as well.

    template <typename U> Matrix2<T>& operator+= (const Matrix2<U>& rhs);
    template <typename U> Matrix2<T>  operator+  (const Matrix2<U>& rhs);

    template <typename U> Matrix2<T>& operator-=(const Matrix2<U>& rhs);
    template <typename U> Matrix2<T>  operator- (const Matrix2<U>& rhs);

    template <typename U> Matrix2<T>& operator*=(const Matrix2<U>& rhs);
    template <typename U> Matrix2<T>  operator* (const Matrix2<U>& rhs);

    template <typename U> Matrix2<T>& operator/=(const Matrix2<U>& rhs);
    template <typename U> Matrix2<T>  operator/ (const Matrix2<U>& rhs);

    template <typename U> Matrix2<T>& operator*= (const Vector<U>& vector);
    // operator*(Vector<U>) is defined as a global operator function.

    template <typename U> Matrix2<T>& operator+= (const U& constant);
    template <typename U> Matrix2<T>  operator+  (const U& constant);

    template <typename U> Matrix2<T>& operator-= (const U& constant);
    template <typename U> Matrix2<T>  operator-  (const U& constant);

    template <typename U> Matrix2<T>& operator*= (const U& constant);
    template <typename U> Matrix2<T>  operator*  (const U& constant);

    template <typename U> Matrix2<T>& operator/= (const U& constant);
    template <typename U> Matrix2<T>  operator/  (const U& constant);

// TODO
//  }

};

#include "Matrix2.tpp"