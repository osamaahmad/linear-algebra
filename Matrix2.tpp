#include <stdexcept>

#include "Matrix2.hpp"
#include "Vector.hpp"
#include "MatrixRowsColumns.hpp"
#include "LinearEquationsSolver.tpp"
#include "MatrixDeterminant.tpp"

template <typename T>
Matrix2<T> Matrix2<T>::transpose() const
{
    Matrix2<T> result(columns(), rows());

    for (int i = 0; i < rows(); i++)
        for (int j = 0; j < columns(); j++)
            result[j][i] = (*this)[i][j];

    return result;
}

template <typename T>
Matrix2<T> Matrix2<T>::identity(int n)
{
    Matrix2<T> result(n, n);
    for (int i = 0; i < n; i++)
        result[i][i] = 1;
    return result;
}

template <typename T>
Matrix2<T> Matrix2<T>::identity() const
{
    if (!is_square())
        throw "TODO";
    return identity(rows());
}

template <typename T> template <typename U>
Matrix2<T> Matrix2<T>::augmented_with(const Matrix2<U> &rhs) const
{
    if (rows() != rhs.rows())
        throw std::invalid_argument("The two matrices don't have the same number of rows");

    Matrix2<T> result(rows(), columns() + rhs.columns());

    for (int i = 0; i < rows(); i++)
    {
        for (int j = 0; j < columns(); j++)
            result[i][j] = (*this)[i][j];

        int offset = columns();
        for (int j = 0; j < rhs.columns(); j++)
            result[i][j + offset] = rhs[i][j];
    }

    return result;
}

template <typename T>
Matrix2<T> Matrix2<T>::split_left(int split_point) const
{
    Matrix2<T> result(rows(), split_point);
    for (int i = 0; i < rows(); i++)
        for (int j = 0; j < split_point; j++)
            result[i][j] = (*this)[i][j];
    return result;
}

template <typename T>
Matrix2<T> Matrix2<T>::split_right(int split_point) const
{
    Matrix2<T> result(rows(), columns() - split_point);
    for (int i = 0; i < rows(); i++)
        for (int j = 0, k = split_point; k < columns(); j++, k++)
            result[i][j] = (*this)[i][k];
    return result;
}

template <typename T>
MatrixRow<T> Matrix2<T>::row(int row_index) {
    return MatrixRow<T>(*this, row_index);
}

template <typename T>
MatrixColumn<T> Matrix2<T>::column(int column_index) {
    return MatrixColumn<T>(*this, column_index);
}

template <typename T>
void Matrix2<T>::reset()
{
    for (int i = 0; i < size(); i++)
        linear_array()[i] = T();
}

template <typename T>
Matrix2<T>& Matrix2<T>::operator=(const Matrix2<T>& rhs)
{
    if (&rhs == this)
        return *this;

    rows_count = rhs.rows();
    columns_count = rhs.columns();

    delete[] data;
    data = new T[size()];

    for (int i = 0; i < size(); i++)
        linear_array()[i] = rhs.linear_array()[i];

    return *this;
}

template <typename T>
Matrix2<T>& Matrix2<T>::operator=(Matrix2<T>&& rhs) noexcept
{
    rows_count = rhs.rows();
    columns_count = rhs.columns();
    std::swap(data, rhs.data);
    return *this;
}

template <typename T> template<typename U>
Matrix2<T>& Matrix2<T>::operator=(const Matrix2<U>& rhs)
{
    rows_count = rhs.rows();
    columns_count = rhs.columns();

    delete[] data;
    data = new T[size()];

    for (int i = 0; i < size(); i++)
        linear_array()[i] = rhs.linear_array()[i];

    return *this;
}

template <typename T> template<typename U>
bool Matrix2<T>::operator==(const Matrix2<U>& rhs) const
{
    if (!has_same_dimensions(rhs))
        return false;

    for (int i = 0; i < rows(); i++)
        for (int j = 0; j < columns(); j++)
            if ((*this)[i][j] != rhs[i][j])
                return false;

    return true;
}

template <typename T> template<typename U>
bool Matrix2<T>::is_equal(const Matrix2<U>& rhs, double tolerance) const
{
    if (!has_same_dimensions(rhs))
        return false;

    tolerance *= tolerance;
    for (int i = 0; i < rows(); i++) {
        for (int j = 0; j < columns(); j++)
        {
            int diff = (*this)[i][j] - rhs[i][j];
            diff *= diff;

            if (diff > tolerance)
                return false;
        }
    }

    return true;
}

template <typename T>
Matrix2<T>::operator Vector<T>() const
{
    if (columns() != 1)
        throw std::invalid_argument("Matrix doesn't have exactly one column, and can't be casted to a vector");

    Vector<T> result;
    result.dimensions = rows();
    result.data = data;

    return result;
}

template <typename T>
T* Matrix2<T>::operator[](int row) {
    return &data[row * columns()];
}

template <typename T>
const T* Matrix2<T>::operator[](int row) const {
    return &data[row * columns()];
}

template <typename T> template <typename U>
Matrix2<T>& Matrix2<T>::operator+=(const Matrix2<U>& rhs)
{
    Matrix2& lhs = *this;

    if (!lhs.has_same_dimensions(rhs)) {
        throw "TODO";
    }

    for (int i = 0; i < size(); i++)
        lhs.data[i] += rhs.linear_array()[i];

    return *this;
}

template <typename T> template <typename U>
Matrix2<T> Matrix2<T>::operator+(const Matrix2<U>& rhs)
{
    Matrix2& lhs = *this;
    Matrix2 result(lhs);
    result += rhs;
    return result;
}

template <typename T> template <typename U>
Matrix2<T>& Matrix2<T>::operator-=(const Matrix2<U>& rhs)
{
    Matrix2& lhs = *this;

    if (!lhs.has_same_dimensions(rhs)) {
        throw "TODO";
    }

    for (int i = 0; i < size(); i++)
        lhs.data[i] -= rhs.linear_array()[i];

    return *this;
}

template <typename T> template <typename U>
Matrix2<T> Matrix2<T>::operator-(const Matrix2<U>& rhs)
{
    Matrix2& lhs = *this;
    Matrix2 result(lhs);
    result -= rhs;
    return result;
}

template <typename T> template <typename U>
Matrix2<T>& Matrix2<T>::operator*=(const Matrix2<U>& rhs)
{
    Matrix2& lhs = *this;
    *this = lhs * rhs;
    return *this;
}

template <typename T> template <typename U>
Matrix2<T> Matrix2<T>::operator*(const Matrix2<U>& rhs)
{
    Matrix2& lhs = *this;

    if (!lhs.template is_multipliable(rhs)) {
        throw "TODO";
    }

    Matrix2<T> result(lhs.rows(), rhs.columns());

    // TODO optimize this.
    for (int i = 0; i < lhs.rows(); i++)
        for (int j = 0; j < rhs.columns(); j++)
            for (int k = 0; k < rhs.rows(); k++)
                result[i][j] += lhs[i][k] * rhs[k][j];

    return result;
}

template<typename T> template<typename U>
Matrix2<T>& Matrix2<T>::operator*=(const Vector<U> &vector)
{
    *this = *this * vector;
    return *this;
}

template<typename T, typename U>
Vector<T> operator*(const Matrix2<T>& matrix, const Vector<U> &vector)
{
    if (matrix.columns() != vector.size())
        throw std::invalid_argument("Size of the vector is not equal to the number of columns of the matrix");

    Vector<T> result(matrix.rows());

    for (int i = 0; i < matrix.rows(); i++)
    {
        T sum = 0;
        for (int j = 0; j < matrix.columns(); j++)
            sum += matrix[i][j] * vector[j];
        result[i] = sum;
    }

    return result;
}

template <typename T> template <typename U>
Matrix2<T>& Matrix2<T>::operator/=(const Matrix2<U>& rhs)
{
    Matrix2& lhs = *this;
    lhs *= rhs.inverse();
    return *this;
}

template <typename T> template <typename U>
Matrix2<T> Matrix2<T>::operator/(const Matrix2<U>& rhs)
{
    Matrix2& lhs = *this;

    // Dimensions of the inverse of matrix A
    //  are the same as the dimensions of A.
    if (!lhs.template is_multipliable(rhs)) {
        throw "TODO";
    }

    return *this * rhs.inverse();
}

template<typename T> template<typename U>
Matrix2<T>& Matrix2<T>::operator+=(const U &constant)
{
    for (int i = 0; i < size(); i++)
        linear_array()[i] += constant;

    return *this;
}

template<typename T> template<typename U>
Matrix2<T> Matrix2<T>::operator+(const U &constant)
{
    Matrix2<T> result(*this);
    result += constant;
    return result;
}

template<typename T, typename U>
Matrix2<U> operator+(const U &constant, const Matrix2<T>& matrix)
{
    Matrix2<U> result(matrix);
    result += constant;
    return result;
}

template<typename T> template<typename U>
Matrix2<T>& Matrix2<T>::operator-=(const U &constant)
{
    for (int i = 0; i < size(); i++)
        linear_array()[i] -= constant;

    return *this;
}

template<typename T> template<typename U>
Matrix2<T> Matrix2<T>::operator-(const U &constant)
{
    Matrix2<T> result(*this);
    result -= constant;
    return result;
}

template<typename T, typename U>
Matrix2<U> operator-(const U &constant, const Matrix2<T>& matrix)
{
    Matrix2<U> result(matrix);
    result -= constant;
    return result;
}

template<typename T> template<typename U>
Matrix2<T>& Matrix2<T>::operator*=(const U &constant)
{
    for (int i = 0; i < size(); i++)
        linear_array()[i] *= constant;

    return *this;
}

template<typename T> template<typename U>
Matrix2<T> Matrix2<T>::operator*(const U &constant)
{
    Matrix2<T> result(*this);
    result *= constant;
    return result;
}

template<typename T, typename U>
Matrix2<U> operator*(const U &constant, const Matrix2<T>& matrix)
{
    Matrix2<U> result(matrix);
    result *= constant;
    return result;
}

template<typename T> template<typename U>
Matrix2<T>& Matrix2<T>::operator/=(const U &constant)
{
    for (int i = 0; i < size(); i++)
        linear_array()[i] /= constant;

    return *this;
}

template<typename T> template<typename U>
Matrix2<T> Matrix2<T>::operator/(const U &constant)
{
    Matrix2<T> result(*this);
    result /= constant;
    return result;
}