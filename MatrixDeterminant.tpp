#include "Matrix2.hpp"

// TODO use another method to avoid the
//  creation of multiple new matrices.

template <typename T>
Matrix2<T> get_submatrix(const Matrix2<T>& matrix, int exclude_col)
{
    Matrix2<T> result(matrix.rows() - 1, matrix.columns() - 1);

    for (int i = 0; i < result.rows(); i++) {
        for (int j = 0; j < result.columns(); j++)
        {
            int col_offset = j >= exclude_col;

            // 0th row in the original matrix is excluded.
            result[i][j] = matrix[i + 1][j + col_offset];
        }
    }

    return result;
}

template <typename T>
T compute_determinant(const Matrix2<T>& matrix)
{
    // will assume that the
    // provided matrix is square.
    if (matrix.rows() == 2) {
        return (matrix[0][0] * matrix[1][1]) - (matrix[0][1] * matrix[1][0]);
    }

    T result = 0;
    int sign = 1;

    for (int i = 0; i < matrix.rows(); i++)
    {
        T submatrix_determinant = compute_determinant(get_submatrix(matrix, i));
        result += matrix[0][i] * submatrix_determinant * sign;
        sign = -sign;
    }

    return result;
}

template <typename T>
T Matrix2<T>::determinant() const
{
    if (!is_square())
        throw std::invalid_argument("Only square matrices can have a determinant");
    return compute_determinant(*this);
}