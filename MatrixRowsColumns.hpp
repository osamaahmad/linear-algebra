#include "Matrix2.hpp"
#include <utility>

// Since the data type of the numbers (T) might be expensive to copy,
//  it might be a good idea to take them as a const reference, but this
//  will introduce a problem if the parameter is an element of the current
//  row/column. For this reason, the parameters are taken by value.

template <typename T>
class MatrixRow
{
    Matrix2<T>& matrix;
    int row_index;

public:

    template <typename U> MatrixRow<T>& operator+=(U constant);
    template <typename U> MatrixRow<T>& operator-=(U constant);
    template <typename U> MatrixRow<T>& operator*=(U constant);
    template <typename U> MatrixRow<T>& operator/=(U constant);
    T& operator[](int i) { return matrix[row_index][i]; }
    const T& operator[](int i) const { return matrix[row_index][i]; }

    template <typename U> bool operator==(const MatrixRow<U>& rhs);

    MatrixRow<T>& add_row(int other_row, const T& multiplier = 1);
    MatrixRow<T>& swap_with_row(int other_row);

    int size() const { return matrix.columns(); };

    MatrixRow(Matrix2<T>& matrix, int row_index)
            : matrix(matrix), row_index(row_index) {}
};

template <typename T> template <typename U>
MatrixRow<T>& MatrixRow<T>::operator+=(U constant)
{
    for (int i = 0; i < size(); i++)
        matrix[row_index][i] += constant;
    return *this;
}

template <typename T> template <typename U>
MatrixRow<T>& MatrixRow<T>::operator-=(U constant)
{
    for (int i = 0; i < size(); i++)
        matrix[row_index][i] -= constant;
    return *this;
}

template <typename T> template <typename U>
MatrixRow<T>& MatrixRow<T>::operator*=(U constant)
{
    for (int i = 0; i < size(); i++)
        matrix[row_index][i] *= constant;
    return *this;
}

template <typename T> template <typename U>
MatrixRow<T>& MatrixRow<T>::operator/=(U constant)
{
    for (int i = 0; i < size(); i++)
        matrix[row_index][i] /= constant;
    return *this;
}

template <typename T> template <typename U>
bool MatrixRow<T>::operator==(const MatrixRow<U>& rhs)
{
    if (size() != rhs.size())
        return false;

    for (int i = 0; i < size(); i++)
        if ((*this)[i] != rhs[i])
            return false;

    return true;
}

template <typename T>
MatrixRow<T>& MatrixRow<T>::add_row(int other_row, const T& multiplier)
{
    for (int i = 0; i < size(); i++)
        (*this)[i] += matrix[other_row][i] * multiplier;
    return *this;
}

template <typename T>
MatrixRow<T>& MatrixRow<T>::swap_with_row(int other_row)
{
    for (int i = 0; i < size(); i++)
        std::swap((*this)[i], matrix[other_row][i]);
    return *this;
}

template <typename T>
class MatrixColumn
{
    Matrix2<T>& matrix;
    int column_index;

public:

    template <typename U> MatrixColumn<T>& operator+=(U constant);
    template <typename U> MatrixColumn<T>& operator-=(U constant);
    template <typename U> MatrixColumn<T>& operator*=(U constant);
    template <typename U> MatrixColumn<T>& operator/=(U constant);
    T& operator[](int i) { return matrix[i][column_index]; }
    const T& operator[](int i) const { return matrix[i][column_index]; }

    template <typename U> bool operator==(const MatrixColumn<U>& rhs);

    int size() const { return matrix.rows(); };

    MatrixColumn(Matrix2<T>& matrix, int column_index)
        : matrix(matrix), column_index(column_index) {}
};

template <typename T> template <typename U>
MatrixColumn<T>& MatrixColumn<T>::operator+=(U constant)
{
    for (int i = 0; i < size(); i++)
        matrix[i][column_index] += constant;
    return *this;
}

template <typename T> template <typename U>
MatrixColumn<T>& MatrixColumn<T>::operator-=(U constant)
{
    for (int i = 0; i < size(); i++)
        matrix[i][column_index] -= constant;
    return *this;
}

template <typename T> template <typename U>
MatrixColumn<T>& MatrixColumn<T>::operator*=(U constant)
{
    for (int i = 0; i < size(); i++)
        matrix[i][column_index] *= constant;
    return *this;
}

template <typename T> template <typename U>
MatrixColumn<T>& MatrixColumn<T>::operator/=(U constant)
{
    for (int i = 0; i < size(); i++)
        matrix[i][column_index] /= constant;
    return *this;
}

template <typename T> template <typename U>
bool MatrixColumn<T>::operator==(const MatrixColumn<U>& rhs)
{
    if (size() != rhs.size())
        return false;

    for (int i = 0; i < size(); i++)
        if ((*this)[i] != rhs[i])
            return false;

    return true;
}
