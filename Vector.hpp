#pragma once
#include "Matrix2.hpp"

template <typename T>
class Vector
{
    friend class Matrix2<T>;

    int dimensions;
    T* data;

public:

    Vector() : dimensions(0), data(nullptr) {}

    Vector(int dimensions)
        : dimensions(dimensions), data(new T[size()])
    {
        reset();
    }

    Vector(std::initializer_list<T> vector)
        : dimensions(vector.size()), data(new T[size()])
    {
        auto item = vector.begin();
        for (int i = 0; i < size(); i++)
            (*this)[i] = *item, item++;
    }

    template <typename U>
    Vector(int dimensions, const U& data)
        : dimensions(dimensions), data(new T[size()])
    {
        for (int i = 0; i < size(); i++)
            (*this)[i] = data[i];
    }

    template <typename U>
    Vector(const Vector<U>& vector)
        : dimensions(vector.size()), data(new T[size()])
    {
        for (int i = 0; i < size(); i++)
            (*this)[i] = vector[i];
    }

    Vector(const Vector<T>& vector)
        : dimensions(vector.size()), data(new T[size()])
    {
        for (int i = 0; i < size(); i++)
            (*this)[i] = vector[i];
    }

    Vector(Vector<T>&& vector) noexcept
    {
        data = vector.data;
        vector.data = nullptr;

        dimensions = vector.dimensions;
    }

//    template <typename U>
//    Vector(const U& vector)
//        : dimensions(vector.size()), data(new T[size()])
//    {
//        for (int i = 0; i < size(); i++)
//        (*this)[i] = vector[i];
//    }

    int size() const { return dimensions; }

    void reset();

    T norm() const;
    Vector<T>& normalize();
    Vector<T>  normalized() const;

    template <typename U> T dot(const Vector<U>& rhs);
    template <typename U> Vector<T> cross(const Vector<U>& rhs);

    template <typename U> Vector<T>& transform(const Matrix2<U>& transformation);
    template <typename U> Vector<T> transformed(const Matrix2<U>& transformation) const;

    operator Matrix2<T>() const;

    T& operator[](int i);
    const T& operator[](int i) const;

    Vector<T>& operator=(const Vector<T>& rhs);
    Vector<T>& operator=(Vector<T>&& rhs) noexcept;
    template <typename U> Vector<T>& operator=(const Vector<U>& rhs);

    template<typename U> bool is_equal(const Vector<U>& rhs, double tolerance) const;
    template <typename U> bool operator==(const Vector<U>& rhs) const;

    template <typename U> Vector<T>& operator+= (const Vector<U>& rhs);
    template <typename U> Vector<T>  operator+  (const Vector<U>& rhs);

    template <typename U> Vector<T>& operator-=(const Vector<U>& rhs);
    template <typename U> Vector<T>  operator- (const Vector<U>& rhs);

    template <typename U> Vector<T>& operator+= (const U& constant);
    template <typename U> Vector<T>  operator+  (const U& constant);

    template <typename U> Vector<T>& operator-= (const U& constant);
    template <typename U> Vector<T>  operator-  (const U& constant);

    template <typename U> Vector<T>& operator*= (const U& constant);
    template <typename U> Vector<T>  operator*  (const U& constant);

    template <typename U> Vector<T>& operator/= (const U& constant);
    template <typename U> Vector<T>  operator/  (const U& constant);
};

#include "Vector.tpp"