#include "Vector.hpp"
#include "stdexcept"
#include <cmath>

template <typename T>
void Vector<T>::reset()
{
    for (int i = 0; i < size(); i++)
        (*this)[i] = T();
}

template<typename T>
T Vector<T>::norm() const
{
    T sum = 0;

    for (int i = 0; i < size(); i++)
        sum += (*this)[i] * (*this)[i];

    return std::sqrt(sum);
}

template <typename T>
Vector<T>& Vector<T>::normalize() {
    *this /= norm();
}

template <typename T>
Vector<T> Vector<T>::normalized() const
{
    Vector<T> result = *this;
    result /= norm();
    return result;
}

template <typename T> template <typename U>
T Vector<T>::dot(const Vector<U>& rhs)
{
    if (size() != rhs.size())
        throw std::invalid_argument("The two vectors have different sizes");

    T result = 0;

    for (int i = 0; i < size(); i++)
        result += (*this)[i] * rhs[i];

    return result;
}

template <typename T> template <typename U>
Vector<T> Vector<T>::cross(const Vector<U>& y)
{
    Vector<T>& x = *this;

    if (x.size() != y.size())
        throw std::invalid_argument("The two vectors have different sizes");

    // Not going to consider the case where the vectors are 7 dimensional.
    if (size() != 3)
        throw std::invalid_argument("Cross-product is supported only for 3 dimensional vectors");

    T arr[3];
    arr[0] = (x[1] * y[2]) - (x[2] * y[1]);
    arr[1] = (x[0] * y[2]) - (x[2] * y[0]);
    arr[2] = (x[0] * y[1]) - (x[1] * y[0]);

    return Vector<T>(3, arr);
}

template <typename T> template <typename U>
Vector<T>& Vector<T>::transform(const Matrix2<U>& transformation)
{
    *this = transformed(transformation);
    return *this;
}

template <typename T> template <typename U>
Vector<T> Vector<T>::transformed(const Matrix2<U>& transformation) const
{
    if (transformation.columns() != size())
        throw std::invalid_argument("Size of the vector is not equal to the number of columns of the matrix");

    Vector<T> result(transformation.rows());

    for (int i = 0; i < transformation.rows(); i++)
    {
        T sum = 0;
        for (int j = 0; j < transformation.columns(); j++)
            sum += transformation[i][j] * (*this)[j];
        result[i] = sum;
    }

    return result;
}

template <typename T>
Vector<T>::operator Matrix2<T>() const
{
    Matrix2<T> matrix;
    matrix.data = data;
    matrix.rows_count = size();
    matrix.columns_count = 1;
    return matrix;
}

template <typename T>
Vector<T>& Vector<T>::operator=(const Vector<T>& rhs)
{
    if (&rhs == this)
        return *this;

    dimensions = rhs.dimensions;

    delete[] data;
    data = new T[size()];

    for (int i = 0; i < size(); i++)
        (*this)[i] = rhs[i];

    return *this;
}

template <typename T>
Vector<T>& Vector<T>::operator=(Vector<T>&& rhs) noexcept
{
    dimensions = rhs.dimensions;
    std::swap(data, rhs.data);
}

template <typename T> template<typename U>
Vector<T>& Vector<T>::operator=(const Vector<U>& rhs)
{
    dimensions = rhs.size();

    delete[] data;
    data = new T[size()];

    for (int i = 0; i < size(); i++)
        (*this)[i] = rhs[i];

    return *this;
}

template <typename T> template<typename U>
bool Vector<T>::operator==(const Vector<U>& rhs) const
{
    if (size() != rhs.size())
        throw std::invalid_argument("The two vectors have different sizes");

    for (int i = 0; i < size(); i++)
            if ((*this)[i] != rhs[i])
                return false;

    return true;
}

template <typename T> template<typename U>
bool Vector<T>::is_equal(const Vector<U>& rhs, double tolerance) const
{
    if (size() != rhs.size())
        return false;

    tolerance *= tolerance;
    for (int i = 0; i < size(); i++)
    {
        int diff = (*this)[i] - rhs[i];
        diff *= diff;

        if (diff > tolerance)
            return false;
    }

    return true;
}

template <typename T>
T& Vector<T>::operator[](int i) {
    return data[i];
}

template <typename T>
const T& Vector<T>::operator[](int i) const {
    return data[i];
}

template <typename T> template <typename U>
Vector<T>& Vector<T>::operator+=(const Vector<U>& rhs)
{
    if (size() != rhs.size())
        throw std::invalid_argument("The two vectors have different sizes");

    for (int i = 0; i < size(); i++)
        (*this)[i] += rhs[i];

    return *this;
}

template <typename T> template <typename U>
Vector<T> Vector<T>::operator+(const Vector<U>& rhs)
{
    Vector& lhs = *this;
    Vector result(lhs);
    result += rhs;
    return result;
}

template <typename T> template <typename U>
Vector<T>& Vector<T>::operator-=(const Vector<U>& rhs)
{
    if (size() != rhs.size())
        throw std::invalid_argument("The two vectors have different sizes");

    for (int i = 0; i < size(); i++)
        (*this)[i] -= rhs[i];

    return *this;
}

template <typename T> template <typename U>
Vector<T> Vector<T>::operator-(const Vector<U>& rhs)
{
    Vector& lhs = *this;
    Vector result(lhs);
    result -= rhs;
    return result;
}

template<typename T> template<typename U>
Vector<T>& Vector<T>::operator+=(const U &constant)
{
    for (int i = 0; i < size(); i++)
        (*this)[i] += constant;

    return *this;
}

template<typename T> template<typename U>
Vector<T> Vector<T>::operator+(const U &constant)
{
    Vector<T> result(*this);
    result += constant;
    return result;
}

template<typename T, typename U>
Vector<U> operator+(const U &constant, const Vector<T>& matrix)
{
    Vector<U> result(matrix);
    result += constant;
    return result;
}

template<typename T> template<typename U>
Vector<T>& Vector<T>::operator-=(const U &constant)
{
    for (int i = 0; i < size(); i++)
        (*this)[i] -= constant;

    return *this;
}

template<typename T> template<typename U>
Vector<T> Vector<T>::operator-(const U &constant)
{
    Vector<T> result(*this);
    result -= constant;
    return result;
}

template<typename T, typename U>
Vector<U> operator-(const U &constant, const Vector<T>& matrix)
{
    Vector<U> result(matrix);
    result -= constant;
    return result;
}

template<typename T> template<typename U>
Vector<T>& Vector<T>::operator*=(const U &constant)
{
    for (int i = 0; i < size(); i++)
        (*this)[i] *= constant;

    return *this;
}

template<typename T> template<typename U>
Vector<T> Vector<T>::operator*(const U &constant)
{
    Vector<T> result(*this);
    result *= constant;
    return result;
}

template<typename T, typename U>
Vector<U> operator*(const U &constant, const Vector<T>& vector)
{
    Vector<U> result(vector);
    result *= constant;
    return result;
}

template<typename T> template<typename U>
Vector<T>& Vector<T>::operator/=(const U &constant)
{
    for (int i = 0; i < size(); i++)
        (*this)[i] /= constant;

    return *this;
}

template<typename T> template<typename U>
Vector<T> Vector<T>::operator/(const U &constant)
{
    Vector<T> result(*this);
    result /= constant;
    return result;
}